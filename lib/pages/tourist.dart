import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/screens/profile_screen.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? userId;
  String? profilePhotoURL;

  @override
  void initState() {
    super.initState();
    fetchCurrentUser();
  }

  Future<void> fetchCurrentUser() async {
    try {
      // Get the current user
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        // If the user is logged in, store the user ID
        setState(() {
          userId = user.uid;
        });
        // Fetch the profile photo URL
        await fetchProfilePhotoURL(userId!);
      } else {
        print('Error: User not logged in.');
      }
    } catch (error) {
      print('Error fetching current user: $error');
    }
  }

  Future<void> fetchProfilePhotoURL(String userId) async {
    try {
      // Create a reference to the profile photo in Firebase Storage
      Reference ref = FirebaseStorage.instance
          .ref()
          .child('profile_photos')
          .child(userId)
          .child('profile_photo.jpg');

      // Get the download URL for the profile photo
      String downloadURL = await ref.getDownloadURL();

      // Update the state with the download URL
      setState(() {
        profilePhotoURL = downloadURL;
      });
    } catch (error) {
      print('Error fetching profile photo: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      appBar: AppBar(
        titleSpacing: 0,
        iconTheme: const IconThemeData(color: AppColors.whiteColor),
        title: Padding(
          padding: EdgeInsets.only(right: 6.w, top: 8.h),
          child: Container(
            height: 50.h,
            decoration: BoxDecoration(
                color: AppColors.backgroundLight,
                borderRadius: BorderRadius.circular(70.r)),
            child: Padding(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                    icon: const Icon(Icons.search),
                    color: AppColors.whiteColor,
                    onPressed: () {},
                  ),
                  Expanded(
                      child: TextField(
                    onChanged: (value) {},
                    decoration: const InputDecoration(
                        focusColor: Colors.black,
                        hintText: "Explore...",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: AppColors.whiteColor),
                        border: InputBorder.none),
                  )),
                  IconButton(
                    icon: const Icon(Icons.close),
                    color: AppColors.whiteColor,
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          ),
        ),
        backgroundColor: AppColors.backgroundDark,
        actions: [
          Padding(
            padding: EdgeInsets.only(left: 8.w, right: 8.w, top: 8.h),
            child: InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ProfileScreen()));
              },
              child: CircleAvatar(
                backgroundColor: AppColors.backgroundLight,
                radius: 28.r,
                child: profilePhotoURL != null
                    ? ClipOval(
                        child: Image.network(
                          profilePhotoURL!,
                          width: 200.w,
                          height: 200.h,
                          fit: BoxFit.cover,
                        ),
                      )
                    : CircleAvatar(
                        radius: 28.r,
                        backgroundImage: const NetworkImage(
                            "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"),
                      ),
              ),
            ),
          )
        ],
      ),
      drawer: Drawer(
        backgroundColor: AppColors.backgroundLight,
        child: ListView(
          children: [
            ListTile(
              leading: const Icon(
                Icons.login,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Login",
              ),
              onTap: () {
                context.push(Routes.loginScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.book_online,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Book Trip",
              ),
              onTap: () {
                context.push(Routes.bookScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.park,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Hill Stations",
              ),
              onTap: () {
                context.push(Routes.hillScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.temple_hindu,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Religious",
              ),
              onTap: () {
                context.push(Routes.religiousScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.forest,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Wild Life",
              ),
              onTap: () {
                context.push(Routes.wildScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.festival,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Cultural",
              ),
              onTap: () {
                context.push(Routes.culturalScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.paragliding,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Adventure",
              ),
              onTap: () {
                context.push(Routes.adventureScreen);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.fort,
                color: AppColors.whiteColor,
              ),
              title: const TextWidget(
                text: "Historical",
              ),
              onTap: () {
                context.push(Routes.historyScreen);
              },
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w, right: 8.w),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () {
                        context.push(Routes.hillScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Hill Stations"),
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.religiousScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Religious"),
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.wildScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Wild Life"),
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.culturalScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Cultural"),
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.adventureScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Adventure"),
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.historyScreen);
                      },
                      child: const TextContainer(
                          height: 60, width: 300, text: "Historical"),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Hill Stations",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.hillScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w, right: 8.w),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.shimlaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.manaliImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.ootyImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.matheranImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.munnarImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.darjilingImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.kodaikanalImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.srinagarImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.mussoorieImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.nainitalImage)),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Religious",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.religiousScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w, right: 8.w),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.vaishnodeviImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.goldenImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.sunImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.jagannathImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.rankapurImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.dwarkaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.amarnathImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.mathuraImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.maduraiImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.somnathImage)),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Wild Life",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.wildScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w, right: 8.w),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.jimImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.kaziImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.rantaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.girImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.hemisImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.nagarImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.keolaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.satpuraImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.bandhavImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.periyarImage)),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Cultural",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.culturalScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Padding(
                padding: EdgeInsets.only(left: 8.w, right: 8.w),
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.amritImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.tamilImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.mysoreImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.hampiImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.goaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.kolkataImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.cholaImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.khajurImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.rajsthanImage)),
                    ImageContainer(
                        height: 300,
                        width: 300,
                        image: AssetImage(AppImages.lucknowImage)),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Adventure",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.adventureScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            const SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.riverImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.paraImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.scubaImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.paragImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.skiingImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.duneImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.bungeImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.skyImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.trekImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.gonImage)),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 18.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Historical",
                  ),
                  InkWell(
                      onTap: () {
                        context.push(Routes.historyScreen);
                      },
                      child: const Icon(
                        Icons.arrow_forward,
                        color: AppColors.whiteColor,
                      ))
                ],
              ),
            ),
            const SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.tajImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.agraImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.redImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.minarImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.sikriImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.hawaImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.victoriaImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.tombImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.bagImage)),
                  ImageContainer(
                      height: 300,
                      width: 300,
                      image: AssetImage(AppImages.stupImage)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
