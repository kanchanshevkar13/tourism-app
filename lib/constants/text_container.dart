import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextContainer extends StatelessWidget {
  final double height;
  final double width;
  final String text;
  const TextContainer({
    super.key,
    required this.height,
    required this.width,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10.w, right: 10.w, top: 18.h, bottom: 10.w),
      child: Container(
          height: height.h,
          width: width.w,
          decoration: BoxDecoration(
            color: AppColors.backgroundLight,
            borderRadius: BorderRadius.circular(50.r),
          ),
          child: Center(
              child: TextWidget(
                 text: text,
                 fontSize: 14,
                 fontWeight: FontWeight.bold,
                 
            //style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold,color: AppColors.whiteColor),
          ))),
    );
  }
}
