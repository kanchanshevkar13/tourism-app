// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase1/constants/app_colors.dart';
// import 'package:firebase1/constants/textwidget.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';

// class MyTrips extends StatefulWidget {
//   const MyTrips({super.key});

//   @override
//   State<MyTrips> createState() => _MyTripsState();
// }

// class _MyTripsState extends State<MyTrips> {
//   String? userId;

//   @override
//   void initState() {
//     super.initState();
//     fetchCurrentUser();
//   }

//   Future<void> fetchCurrentUser() async {
//     try {
//       // Get the current user
//       User? user = FirebaseAuth.instance.currentUser;
//       if (user != null) {
//         // If the user is logged in, store the user ID
//         setState(() {
//           userId = user.uid;
//         });
//       } else {
//         print('Error: User not logged in.');
//       }
//     } catch (error) {
//       print('Error fetching current user: $error');
//     }
//   }

//   Future<void> _deleteTrip(int index) async {
//     User? userId = FirebaseAuth.instance.currentUser;
//     var userDocRef =
//         FirebaseFirestore.instance.collection('users').doc(userId!.uid);

//     var snapshot = await userDocRef.get();
//     if (snapshot.exists) {
//       List<dynamic> trips = List.from(snapshot.data()!['trips']);
//       trips.removeAt(index);

//       await userDocRef.update({'trips': trips});
//       print('Trip at index $index deleted successfully.');
//     } else {
//       print('Document does not exist');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     User? user = FirebaseAuth.instance.currentUser;
//     return Scaffold(
//         backgroundColor: AppColors.backgroundDark,
//         appBar: AppBar(
//             iconTheme: const IconThemeData(color: AppColors.whiteColor),
//             titleSpacing: 0,
//             title: Padding(
//               padding: const EdgeInsets.only(right: 10),
//               child: Container(
//                 height: 50,
//                 decoration: BoxDecoration(
//                     color: AppColors.backgroundLight,
//                     borderRadius: BorderRadius.circular(70)),
//                 child: Padding(
//                   padding: const EdgeInsets.only(left: 8.0),
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       IconButton(
//                         icon: const Icon(Icons.search),
//                         color: AppColors.whiteColor,
//                         onPressed: () {},
//                       ),
//                       Expanded(
//                           child: TextField(
//                         onChanged: (value) {},
//                         decoration: const InputDecoration(
//                             focusColor: Colors.black,
//                             hintText: "My Trips",
//                             hintStyle: TextStyle(
//                                 fontWeight: FontWeight.bold,
//                                 fontSize: 20,
//                                 color: AppColors.whiteColor),
//                             border: InputBorder.none),
//                       )),
//                       IconButton(
//                         icon: const Icon(Icons.close),
//                         color: AppColors.whiteColor,
//                         onPressed: () {},
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             backgroundColor: AppColors.backgroundDark),
//         body: StreamBuilder(
//             stream: FirebaseFirestore.instance
//                 .collection('users')
//                 .doc(user!.uid)
//                 .snapshots(),
//             builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
//               if (snapshot.connectionState == ConnectionState.waiting) {
//                 return const Center(child: CircularProgressIndicator());
//               }

//               if (snapshot.hasError) {
//                 return Text('Error: ${snapshot.error}');
//               }

//               if (!snapshot.hasData || !snapshot.data!.exists) {
//                 return Text('User data not found');
//               }

//               Map<String, dynamic> userData =
//                   snapshot.data!.data() as Map<String, dynamic>;
//               List<dynamic> trips = userData['trips'];

//               return SingleChildScrollView(
//                 child: Column(
//                   children: [
//                     for (int index = 0;
//                         index < trips.length;
//                         index++) // Iterate through each trip
//                       Padding(
//                         padding: const EdgeInsets.all(10),
//                         child:
//                          Container(
//                           width: MediaQuery.of(context).size.width,
//                           height: 210,
//                           decoration: BoxDecoration(
//                             color: AppColors.backgroundLight,
//                             borderRadius: BorderRadius.circular(20),
//                           ),
//                           child: Padding(
//                             padding: const EdgeInsets.only(
//                                 left: 13, top: 8.0, right: 8.0, bottom: 8.0),
//                             child: Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 TextWidget(
//                                   text: 'Name: ${trips[index]['name']}',
//                                 ),
//                                 TextWidget(
//                                   text:
//                                       'Destination: ${trips[index]['destination']}',
//                                 ),
//                                 TextWidget(
//                                   text:
//                                       'Start Date: ${trips[index]['start date']}',
//                                 ),
//                                 TextWidget(
//                                   text: 'End Date: ${trips[index]['end date']}',
//                                 ),
//                                 TextWidget(
//                                   text:
//                                       'Total Members: ${trips[index]['total members']}',
//                                 ),
//                                 ElevatedButton(
//                                   style: ButtonStyle(
//                                       backgroundColor:
//                                           MaterialStateProperty.all<Color>(
//                                               Colors.red)),
//                                   onPressed: () => _deleteTrip(index),
//                                   child: const TextWidget(
//                                     text: 'Cancel Trip',
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       ),
//                   ],
//                 ),
//               );
//             }));
//   }
// }

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

class MyTrips extends StatefulWidget {
  const MyTrips({super.key});

  @override
  State<MyTrips> createState() => _MyTripsState();
}

class _MyTripsState extends State<MyTrips> {
  String? userId;

  @override
  void initState() {
    super.initState();
    fetchCurrentUser();
  }

  Future<void> fetchCurrentUser() async {
    try {
      // Get the current user
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        // If the user is logged in, store the user ID
        setState(() {
          userId = user.uid;
        });
      } else {
        print('Error: User not logged in.');
      }
    } catch (error) {
      print('Error fetching current user: $error');
    }
  }

  Future<void> _deleteTrip(int index) async {
    User? userId = FirebaseAuth.instance.currentUser;
    var userDocRef =
        FirebaseFirestore.instance.collection('users').doc(userId!.uid);

    var snapshot = await userDocRef.get();
    if (snapshot.exists) {
      List<dynamic> trips = List.from(snapshot.data()!['trips']);
      trips.removeAt(index);

      await userDocRef.update({'trips': trips});
      print('Trip at index $index deleted successfully.');
    } else {
      print('Document does not exist');
    }
  }

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
            automaticallyImplyLeading: false,
            title: Row(
              children: [
                CustomIconWidget(
                    onPressed: () {
                      context.pop();
                    },
                    iconData: Icons.arrow_back,
                    color: AppColors.textColor,
                    size: 20),
                const SizedBoxWidget(
                  width: 20,
                ),
                const TextWidget(
                  text: "My Trips",
                  color: AppColors.textColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                )
              ],
            ),
            // Padding(
            //   padding: const EdgeInsets.only(right: 10),
            //   child: Container(
            //     height: 50,
            //     decoration: BoxDecoration(
            //         color: AppColors.backgroundLight,
            //         borderRadius: BorderRadius.circular(70)),
            //     child: Padding(
            //       padding: const EdgeInsets.only(left: 8.0),
            //       child: Row(
            //         crossAxisAlignment: CrossAxisAlignment.center,
            //         children: [
            //           IconButton(
            //             icon: const Icon(Icons.search),
            //             color: AppColors.whiteColor,
            //             onPressed: () {},
            //           ),
            //           Expanded(
            //               child: TextField(
            //             onChanged: (value) {},
            //             decoration: const InputDecoration(
            //                 focusColor: Colors.black,
            //                 hintText: "My Trips",
            //                 hintStyle: TextStyle(
            //                     fontWeight: FontWeight.bold,
            //                     fontSize: 20,
            //                     color: AppColors.whiteColor),
            //                 border: InputBorder.none),
            //           )),
            //           IconButton(
            //             icon: const Icon(Icons.close),
            //             color: AppColors.whiteColor,
            //             onPressed: () {},
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            backgroundColor: AppColors.backgroundDark),
        body: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection('users')
                .doc(user!.uid)
                .snapshots(),
            builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }

              if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              }

              if (!snapshot.hasData || !snapshot.data!.exists) {
                return Text('User data not found');
              }

              Map<String, dynamic> userData =
                  snapshot.data!.data() as Map<String, dynamic>;
              List<dynamic> trips = userData['trips'];

              return SingleChildScrollView(
                child: Column(
                  children: [
                    for (int index = 0;
                        index < trips.length;
                        index++) // Iterate through each trip
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: ExpansionTile(
                          title: TextWidget(
                            text: '${trips[index]['destination']}',
                            color: AppColors.textColor,
                          ),
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 15.w),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      TextWidget(
                                        text:
                                            'Destination: ${trips[index]['destination']}',
                                        color: AppColors.textColor,
                                      ),
                                      TextWidget(
                                        text:
                                            'Start Date: ${trips[index]['start date']}',
                                        color: AppColors.textColor,
                                      ),
                                      TextWidget(
                                        text:
                                            'End Date: ${trips[index]['end date']}',
                                        color: AppColors.textColor,
                                      ),
                                      TextWidget(
                                        text:
                                            'Total Members: ${trips[index]['total members']}',
                                        color: AppColors.textColor,
                                      ),
                                    ],
                                  ),
                                  ElevatedButton(
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all<Color>(
                                                Colors.red)),
                                    onPressed: () => _deleteTrip(index),
                                    child: const TextWidget(
                                      text: 'Cancel Trip',
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              );
            }));
  }
}
