import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Adventure extends StatefulWidget {
  const Adventure({super.key});

  @override
  State<Adventure> createState() => _AdventureState();
}

class _AdventureState extends State<Adventure> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              CustomIconWidget(
                onPressed: () {
                  context.push(Routes.homePage);
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
              ),
              const SizedBoxWidget(
                width: 20,
              ),
              const TextWidget(
                text: "Adventure",
                color: AppColors.textColor,
              )
            ],
          ),

          //title: const TextWidget(text: "Adventure"),
          backgroundColor: AppColors.backgroundDark,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.riverImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "River rafting, Rishikesh"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.paraImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Parasailing, Goa"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.scubaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Scuba diving, Andaman"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.paragImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Paraglinding, Bir billing"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.skiingImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Skiing, Auli"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.duneImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Dune bashing, Jaisalmer"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.duneImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Bungee jumping, Rishikesh"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.skyImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Skydiving, Rishikesh"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.trekImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Trekking, Manali"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.gonImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Gondola ride, Gulumarg"),
              ],
            ),
          ),
        ));
  }
}
