import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Wild extends StatefulWidget {
  const Wild({super.key});

  @override
  State<Wild> createState() => _WildState();
}

class _WildState extends State<Wild> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              CustomIconWidget(
                onPressed: () {
                  context.push(Routes.homePage);
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
              ),
              const SizedBoxWidget(
                width: 20,
              ),
              const TextWidget(
                text: "Wild Life",
                color: AppColors.textColor,
              )
            ],
          ),
          //const TextWidget(text: "Wild Life"),
          backgroundColor: AppColors.backgroundDark,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.jimImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Jim Corbett National Park"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.kaziImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Kazirango"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.rantaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Ranthambore"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.girImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Gir"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.hemisImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Hemis"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.nagarImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Nagarahole"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.keolaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Keoladeo"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.satpuraImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Satpura"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.bandhavImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Bandhavgarh"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.periyarImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Periyar"),
              ],
            ),
          ),
        ));
  }
}
