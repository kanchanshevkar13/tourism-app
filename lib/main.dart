import 'package:firebase1/pages/tourist.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/*void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}*/

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: 'AIzaSyDgdEM0k6v2bCp3BnYVQX4ULkW_lUd0FdU',
      appId: '1:669732755353:android:9f0f6da049222ea534e508',
      messagingSenderId: '669732755353',
      projectId: 'flutter-project-8b5ef',
      storageBucket: 'flutter-project-8b5ef.appspot.com',
      databaseURL: 'https://flutter-project-8b5ef-default-rtdb.firebaseio.com',
      // authDomain: 'flutter-project-8b5ef.firebaseapp.com'
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final appRoutes = Routes();
    return ScreenUtilInit(
      minTextAdapt: true,
      splitScreenMode: true,
      designSize: Size(
        MediaQuery.of(context).size.width,
        MediaQuery.of(context).size.height,
      ),
      child: MaterialApp.router(
        routeInformationParser: appRoutes.router.routeInformationParser,
        routeInformationProvider: appRoutes.router.routeInformationProvider,
        routerDelegate: appRoutes.router.routerDelegate,
        title: "Tourism",
        debugShowCheckedModeBanner: false,
        //theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: Color.fromARGB(255, 58, 86, 138)),
        //home: const SignUpScreen(),
        //home: RealTimeData(),
        //home: const LoginScreen(),
        //home: HomeScreen(),
        //home: HomeScreen(),
        //home: RealTimeData(),
        //home: HomePage(),
        //home: ProfileScreen(),
        //home: LoginScreen(),
      ),
    );
  }
}
