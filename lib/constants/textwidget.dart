// import 'package:firebase1/constants/app_colors.dart';
// import 'package:flutter/material.dart';

// class TextWidget extends StatefulWidget {
//   final String text;
//   const TextWidget({super.key,required this.text,});

//   @override
//   State<TextWidget> createState() => _TextWidgetState();
// }

// class _TextWidgetState extends State<TextWidget> {
//   @override
//   Widget build(BuildContext context) {
//     return Text(
//       widget.text,
//       style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: AppColors.whiteColor),
//     );
//   }
// }

import 'package:firebase1/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class TextWidget extends StatelessWidget {
  const TextWidget({
    super.key,
    required this.text,
    this.fontSize,
    this.color,
    this.textAlign,
    this.fontWeight,
    this.letterSpacing,
    this.textOverflow,
    this.textDecoration,
    this.maxline,
  });

  final String? text;
  final double? fontSize;
  final FontWeight? fontWeight;
  final TextDecoration? textDecoration;
  final Color? color;
  final TextAlign? textAlign;
  final double? letterSpacing;
  final TextOverflow? textOverflow;
  final int? maxline;

  @override
  Widget build(BuildContext context) {
    return Text(
      text!,
      textAlign: textAlign,
      overflow: textOverflow,
      maxLines: maxline,
      style: GoogleFonts.baloo2(
         fontSize: fontSize ?? 14.sp,
         decoration: textDecoration,
          fontWeight: fontWeight,
          color: color ?? AppColors.textColor,
          letterSpacing: letterSpacing,
      )
      // TextStyle(
      //   decoration: textDecoration,
      //   fontSize: fontSize ?? 14.sp,
      //   fontWeight: fontWeight,
      //   color: color ?? AppColors.textColor,
      //   fontFamily: "baloo2",
      //   letterSpacing: letterSpacing,
      // ),
    );
  }
}
