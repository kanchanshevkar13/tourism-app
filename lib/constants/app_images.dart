class AppImages {
  static const String agraImage = 'assets/images/agra.jpg';
  static const String amarnathImage = 'assets/images/amarnath.jpg';
  static const String amritImage = 'assets/images/amrit.jpg';
  static const String backgroundImage = 'assets/images/background.jpg';
  static const String bagImage = 'assets/images/bag.jpg';
  static const String bandhavImage = 'assets/images/bandhav.jpg';
  static const String bungeImage = 'assets/images/bunge.jpg';
  static const String cholaImage = 'assets/images/chola.jpg';
  static const String darjilingImage = 'assets/images/darjiling.jpg';
  static const String backImage = 'assets/images/download (4).jpg';

  static const String duneImage = 'assets/images/dune.jpg';
  static const String dwarkaImage = 'assets/images/dwarka.jpg';
  static const String girImage = 'assets/images/gir.jpg';
  static const String goaImage = 'assets/images/goa.jpg';
  static const String goldenImage = 'assets/images/golden.jpg';
  static const String gonImage = 'assets/images/gon.jpg';
  static const String hampiImage = 'assets/images/hampi.jpg';

  static const String hawaImage = 'assets/images/hawa.jpg';
  static const String hemisImage = 'assets/images/hemis.jpg';
  static const String jagannathImage = 'assets/images/jagannath.jpg';
  static const String jimImage = 'assets/images/jim.jpg';
  static const String kaziImage = 'assets/images/kazi.jpg';
  static const String keolaImage = 'assets/images/keola.jpg';
  static const String khajurImage = 'assets/images/khajur.jpg';

  static const String kodaikanalImage = 'assets/images/kodaikanal.jpg';
  static const String kolkataImage = 'assets/images/kolkata.jpg';
  static const String lucknowImage = 'assets/images/lucknow.jpg';
  static const String maduraiImage = 'assets/images/madurai.jpg';
  static const String manaliImage = 'assets/images/manali.jpg';
  static const String matheranImage = 'assets/images/matheran.jpg';
  static const String mathuraImage = 'assets/images/mathura.jpg';

  static const String minarImage = 'assets/images/minar.jpg';
  static const String munnarImage = 'assets/images/munnar.jpg';
  static const String mussoorieImage = 'assets/images/Mussoorie.jpg';
  static const String mysoreImage = 'assets/images/mysore.jpg';
  static const String nagarImage = 'assets/images/nagar.jpg';
  static const String nainitalImage = 'assets/images/nainital.jpg';
  static const String ootyImage = 'assets/images/ooty.jpg';

  static const String paraImage = 'assets/images/para.jpg';
  static const String paragImage = 'assets/images/parag.jpg';
  static const String periyarImage = 'assets/images/periyar.jpg';
  static const String rajsthanImage = 'assets/images/rajsthan.jpg';
  static const String rankapurImage = 'assets/images/rankapur.jpg';
  static const String rantaImage = 'assets/images/ranta.jpg';
  static const String redImage = 'assets/images/red.jpg';

  static const String riverImage = 'assets/images/river.jpg';
  static const String satpuraImage = 'assets/images/satpura.jpg';
  static const String scubaImage = 'assets/images/scuba.jpg';
  static const String shimlaImage = 'assets/images/shimla.jpg';
  static const String sikriImage = 'assets/images/sikri.jpg';
  static const String skiingImage = 'assets/images/skiing.jpg';
  static const String skyImage = 'assets/images/sky.jpg';
  static const String somnathImage = 'assets/images/somnath.jpg';
  static const String srinagarImage = 'assets/images/srinagar.jpg';
  static const String stupImage = 'assets/images/stup.jpg';

  static const String sunImage = 'assets/images/sun.jpg';
  static const String tajImage = 'assets/images/taj.jpg';
  static const String tamilImage = 'assets/images/tamil.jpg';
  static const String tombImage = 'assets/images/tomb.jpg';
  static const String trekImage = 'assets/images/trek.jpg';
  static const String vaishnodeviImage = 'assets/images/vaishnodevi.jpg';
  static const String victoriaImage = 'assets/images/victoria.jpg';
}
