/*import 'package:firebase1/auth_method.dart';
import 'package:firebase1/screens/login_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          ElevatedButton(
              onPressed: () async {
                await AuthMethod().signOut();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
              child: Text('LogOut'))
        ],
        title: const Text("Tourism Application"),
      ),
      body: Center(
          child: Text(
        "welcome",
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      )),
    );
  }
}*/

import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/pages/tourist.dart';
import 'package:firebase1/screens/book_screen.dart';
import 'package:firebase1/screens/login_screen.dart';
import 'package:firebase1/screens/sign_up_screen.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String? userId;

  @override
  void initState() {
    super.initState();
    //fetchCurrentUser();
  }

  Future<void> fetchCurrentUser() async {
    try {
      // Get the current user
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        // If the user is logged in, store the user ID
        setState(() {
          userId = user.uid;
        });
        context.push(Routes.homePage);
      } else {
        context.push(Routes.loginScreen);
        print('Error: User not logged in.');
      }
    } catch (error) {
      print('Error fetching current user: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset(
            AppImages.backImage, // Replace with your welcome image
            fit: BoxFit.fill,
          ),
          Container(
            color: Colors.black.withOpacity(0.5), // Overlay to darken the image
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Say Hello To  The World..!",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBoxWidget(height: 20),
              const Text(
                "Let's Start Your Vacation",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 28.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBoxWidget(height: 20),
              ElevatedButton(
                onPressed: () {
                  //Navigate to the main part of your application
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => LoginScreen()));
                  fetchCurrentUser();
                },
                child: const Text(
                  'Get Started',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
