import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/custom_button_widget.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:firebase1/widgets/textformfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({super.key});

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmpasswordController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: AppColors.backgroundDark,
        title: Row(
          children: [
            CustomIconWidget(
              onPressed: () {
                context.pop();
              },
              iconData: Icons.arrow_back,
              color: AppColors.textColor,
              size: 20,
            ),
            const SizedBoxWidget(
              width: 20,
            ),
            const TextWidget(
              text: "Reset Passord",
              color: AppColors.textColor,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
      ),
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.all(20.w),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextformFieldWidget(
                hintText: "Enter Your Email",
                controller: emailController,
                textColor: AppColors.textColor,
                hintTextColor: AppColors.lightTextGreyColor,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your email address';
                  }
                  if (!RegExp(
                          r'^[\w-]+(\.[\w-]+)*@(?![0-9_])[a-zA-Z0-9_]+(\.[a-zA-Z]{2,7})$')
                      .hasMatch(value)) {
                    return 'Please enter a valid email address';
                  }
                  return null;
                },
              ),
              const SizedBoxWidget(
                height: 20,
              ),
              TextformFieldWidget(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                hintText: "Enter Your Password",
                controller: passwordController,
                textColor: AppColors.textColor,
                hintTextColor: AppColors.lightTextGreyColor,
                obscureText: true,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  }
                  if (!RegExp(r'^(?=.*[A-Z])').hasMatch(value)) {
                    return 'Password must contain at least one capital letter';
                  }
                  if (!RegExp(r'^(?=.*[!@#$%^&*(),.?":{}|<>])')
                      .hasMatch(value)) {
                    return 'Password must contain at least one special character';
                  }
                  return null;
                },
              ),
              const SizedBoxWidget(
                height: 20,
              ),
              TextformFieldWidget(
                hintText: "Confirm Your Password",
                autovalidateMode: AutovalidateMode.onUserInteraction,
                controller: confirmpasswordController,
                textColor: AppColors.textColor,
                hintTextColor: AppColors.lightTextGreyColor,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please confirm your password';
                  }
                  if (value != passwordController.text) {
                    return 'Passwords do not match';
                  }
                  return null;
                },
                obscureText: true,
              ),
              const SizedBoxWidget(
                height: 30,
              ),
              CustomOutlinedButton(
                borderRadius: 5.r,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    //loginUser();
                  }
                },
                buttonHeight: 50,
                buttonWidth: double.infinity,
                backgroundColor: AppColors.darkGreyColor,
                buttonText: "Reset Password",
                textColor: AppColors.textColor,
                fontSize: 18,
              ),
            ],
          ),
        ),
      )),
    );
  }
}
