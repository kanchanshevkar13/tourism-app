import 'dart:io';
import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/utils/snackbar.dart';
import 'package:firebase1/widgets/custom_button_widget.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  File? selectedImage;
  String? profilePhotoURL;
  String? userId;

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  @override
  void initState() {
    super.initState();
    fetchCurrentUser();
  }

  Future<void> _signOut() async {
    try {
      await _auth.signOut();
      // Navigate to the sign-in screen or any other screen you want after signing out
      // For example:
      // Navigator.of(context).pushReplacement(
      //     MaterialPageRoute(builder: (context) => SignUpScreen()));
      context.push(Routes.signupScreen);
      SnackBarWidget.showSnackbar(context, "You have Logged Out successfully");
      //ShowSnackBar(context, "You have Logged Out successfully");
    } catch (e) {
      print("Failed to sign out: $e");
      // Handle sign-out failure
    }
  }

  Future<void> fetchCurrentUser() async {
    try {
      // Get the current user
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        // If the user is logged in, store the user ID
        setState(() {
          userId = user.uid;
        });
        // Fetch the profile photo URL
        await fetchProfilePhotoURL(userId!);
      } else {
        print('Error: User not logged in.');
      }
    } catch (error) {
      print('Error fetching current user: $error');
    }
  }

  Future<void> fetchProfilePhotoURL(String userId) async {
    try {
      // Create a reference to the profile photo in Firebase Storage
      Reference ref = FirebaseStorage.instance
          .ref()
          .child('profile_photos')
          .child(userId)
          .child('profile_photo.jpg');

      // Get the download URL for the profile photo
      String downloadURL = await ref.getDownloadURL();

      // Update the state with the download URL
      setState(() {
        profilePhotoURL = downloadURL;
      });
    } catch (error) {
      print('Error fetching profile photo: $error');
    }
  }

  Future<void> _deleteAccount(BuildContext context) async {
    try {
      // Get current user
      final user = _auth.currentUser;

      if (user != null) {
        // Delete Firestore documents associated with the user
        await _firestore.collection('users').doc(user.uid).delete();

        // Delete the user account
        await user.delete();

        // Navigate to the login screen or home screen
        // Navigator.of(context).pushReplacement(
        //     MaterialPageRoute(builder: (context) => const SignUpScreen()));
        //ShowSnackBar(context, 'user deleted successfully');

        context.push(Routes.signupScreen);
        SnackBarWidget.showSnackbar(context, "user deleted successfully");
      }
    } catch (e) {
      SnackBarWidget.showSnackbar(context, "failed to delete account");
      //ShowSnackBar(context, 'failed to delete account');
      print('an error occured during deleting data: $e');
    }
  }

  Future<void> deleteProfilePhoto(String userId) async {
    try {
      // Construct the storage reference to the profile photo
      final photoRef = FirebaseStorage.instance
          .ref()
          .child('profile_photos')
          .child(userId)
          .child('profile_photo.jpg');

      // Delete the profile photo from storage
      await photoRef.delete();

      // Delete the profile photo reference from Firestore
      final profileDocRef =
          FirebaseFirestore.instance.collection('users').doc(userId);
      await profileDocRef.update({'profilePhotoUrl': FieldValue.delete()});

      // Navigator.of(context).pop();
      //ShowSnackBar(context, 'profile photo removed successfully');
      context.pop();
      SnackBarWidget.showSnackbar(
          context, "Profile photo removed successfully");
      print(
          'Profile photo deleted successfully from Firebase Storage and Firestore');
    } catch (e) {
      print('Error deleting profile photo: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            CustomIconWidget(
                onPressed: () {
                  context.pop();
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
                size: 20),
            const SizedBoxWidget(
              width: 20,
            ),
            const TextWidget(
              text: "My Profile",
              color: AppColors.textColor,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            )
          ],
        ),
        backgroundColor: AppColors.backgroundDark,
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('users')
            .doc(user!.uid)
            .snapshots(),
        builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }

          if (!snapshot.hasData || !snapshot.data!.exists) {
            return Text('User data not found');
          }

          Map<String, dynamic> data =
              snapshot.data!.data() as Map<String, dynamic>;

          return Column(
            children: [
              Stack(
                children: [
                  profilePhotoURL != null
                      ? Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: CircleAvatar(
                            radius: 100,
                            child: profilePhotoURL != null
                                ? ClipOval(
                                    child: Image.network(
                                      profilePhotoURL!,
                                      width: 200,
                                      height: 200,
                                      fit: BoxFit.cover,
                                    ),
                                  )
                                : const Center(
                                    child: CircularProgressIndicator()),
                          ),
                        )
                      : const Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: CircleAvatar(
                            radius: 100,
                            backgroundImage: NetworkImage(
                                "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"),
                          ),
                        ),
                  Positioned(
                      bottom: -0,
                      //left: 0,
                      //top: 0,
                      right: -0,
                      child: IconButton(
                          onPressed: () {
                            showImagePickerOption(context);
                          },
                          icon: const Icon(
                            Icons.add_a_photo,
                            color: AppColors.whiteColor,
                          )))
                ],
              ),
              const SizedBoxWidget(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextWidget(
                    text: '${data['name']}',
                    color: AppColors.textColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
              const SizedBoxWidget(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.only(left: 50.w, right: 50.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        CustomIconWidget(
                          onPressed: () {},
                          color: AppColors.textColor,
                          iconData: Icons.person,
                          size: 18,
                        ),
                        const SizedBoxWidget(
                          width: 10,
                        ),
                        const TextWidget(
                          text: "Account",
                          color: AppColors.textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        )
                      ],
                    ),
                    const Divider(
                      color: AppColors.textColor,
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            children: [
                              const TextWidget(
                                text: "Edit Profile",
                                color: AppColors.textColor,
                                fontSize: 16,
                              ),
                              const Spacer(),
                              CustomIconWidget(
                                onPressed: () {},
                                color: AppColors.textColor,
                                iconData: Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            children: [
                              const TextWidget(
                                text: "Change Password",
                                color: AppColors.textColor,
                                fontSize: 16,
                              ),
                              const Spacer(),
                              CustomIconWidget(
                                onPressed: () {
                                  context.push(Routes.resetPassword);
                                },
                                color: AppColors.textColor,
                                iconData: Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            children: [
                              const TextWidget(
                                text: "Delete Account",
                                color: AppColors.textColor,
                                fontSize: 16,
                              ),
                              const Spacer(),
                              CustomIconWidget(
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        backgroundColor:
                                            AppColors.backgroundLight,
                                        title: const TextWidget(
                                          text: 'Confirm Delete Account',
                                          fontSize: 18,
                                        ),
                                        content: const TextWidget(
                                          text:
                                              'Are you sure you want to Delete this Account?',
                                          fontSize: 14,
                                        ),
                                        actions: <Widget>[
                                          CustomOutlinedButton(
                                            buttonWidth: 100,
                                            buttonHeight: 30,
                                            borderRadius: 10,
                                            onPressed: () {
                                              context.pop();
                                            },
                                            backgroundColor: Colors.green,
                                            buttonText: "Cancel",
                                          ),
                                          CustomOutlinedButton(
                                            buttonWidth: 100,
                                            buttonHeight: 30,
                                            borderRadius: 10,
                                            onPressed: () {
                                              _deleteAccount(context);
                                            },
                                            backgroundColor: Colors.red,
                                            buttonText: "Delete",
                                          )
                                        ],
                                      );
                                    },
                                  );
                                },
                                color: AppColors.textColor,
                                iconData: Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            children: [
                              const TextWidget(
                                text: "LogOut",
                                color: AppColors.textColor,
                                fontSize: 16,
                              ),
                              const Spacer(),
                              CustomIconWidget(
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        backgroundColor:
                                            AppColors.backgroundLight,
                                        title: const TextWidget(
                                          text: 'Confirm Logout',
                                          fontSize: 18,
                                        ),
                                        content: const TextWidget(
                                          text:
                                              'Are you sure you want to logout?',
                                          fontSize: 14,
                                        ),
                                        actions: <Widget>[
                                          CustomOutlinedButton(
                                            buttonWidth: 100,
                                            buttonHeight: 30,
                                            borderRadius: 10,
                                            onPressed: () {
                                              context.pop();
                                            },
                                            backgroundColor: Colors.green,
                                            buttonText: "Cancel",
                                          ),
                                          CustomOutlinedButton(
                                            buttonWidth: 100,
                                            buttonHeight: 30,
                                            borderRadius: 10,
                                            onPressed: () {
                                              _signOut();
                                            },
                                            backgroundColor: Colors.red,
                                            buttonText: "LogOut",
                                          )
                                        ],
                                      );
                                    },
                                  );
                                },
                                color: AppColors.textColor,
                                iconData: Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBoxWidget(
                      height: 20,
                    ),
                    Row(
                      children: [
                        CustomIconWidget(
                          onPressed: () {},
                          color: AppColors.textColor,
                          iconData: Icons.view_list,
                          size: 18,
                        ),
                        const SizedBoxWidget(
                          width: 10,
                        ),
                        const TextWidget(
                          text: "My Trips",
                          color: AppColors.textColor,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        )
                      ],
                    ),
                    const Divider(
                      color: AppColors.textColor,
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(8.w),
                          child: Row(
                            children: [
                              const TextWidget(
                                text: "View Trips",
                                color: AppColors.textColor,
                                fontSize: 16,
                              ),
                              const Spacer(),
                              CustomIconWidget(
                                onPressed: () {
                                  context.push(Routes.myTripScreen);
                                },
                                color: AppColors.textColor,
                                iconData: Icons.arrow_forward_ios,
                                size: 16,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  void showImagePickerOption(BuildContext context) {
    showModalBottomSheet(
        backgroundColor: AppColors.backgroundLight,
        context: context,
        builder: (Builder) {
          return Padding(
            padding: EdgeInsets.all(18.w),
            child: SizedBoxWidget(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 7.5,
              child: Row(children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      _pickImageFromGallery();
                    },
                    child: const SizedBoxWidget(
                      child: Column(
                        children: [
                          Icon(
                            Icons.image,
                            size: 50,
                            color: AppColors.textColor,
                          ),
                          TextWidget(text: "Gallery")
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      deleteProfilePhoto(userId!);
                    },
                    child: const SizedBoxWidget(
                      child: Column(
                        children: [
                          Icon(
                            Icons.delete,
                            size: 50,
                            color: AppColors.textColor,
                          ),
                          TextWidget(text: "Remove")
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      _pickImageFromCamera();
                    },
                    child: const SizedBoxWidget(
                      child: Column(
                        children: [
                          Icon(
                            Icons.camera_alt,
                            size: 50,
                          ),
                          TextWidget(text: "Camera")
                        ],
                      ),
                    ),
                  ),
                )
              ]),
            ),
          );
        });
  }

  Future<void> _pickImageFromGallery() async {
    final returnImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);

    if (returnImage != null) {
      final croppedImage = await ImageCropper().cropImage(
        sourcePath: returnImage.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      );

      if (croppedImage != null) {
        print('Image cropped: ${croppedImage.path}');

        setState(() {
          selectedImage = File(croppedImage.path);
        });

        try {
          final currentUser = FirebaseAuth.instance.currentUser;
          if (currentUser != null) {
            final ref = firebase_storage.FirebaseStorage.instance
                .ref()
                .child('profile_photos')
                .child(currentUser.uid)
                .child('profile_photo.jpg');

            await ref.putFile(selectedImage!);

            final downloadURL = await ref.getDownloadURL();

            await FirebaseFirestore.instance
                .collection('users')
                .doc(currentUser.uid)
                .update({'profilePhotoUrl': downloadURL});

            print(
                'Image uploaded to Firebase Storage and URL saved to Firestore.');
          } else {
            print('Error: currentUser is null.');
          }
        } catch (error) {
          print('Error uploading image: $error');
        }
      } else {
        print('Error: Cropped image is null.');
      }
    } else {
      print('Error: No image selected.');
    }

    Navigator.of(context).pop();
  }

  Future _pickImageFromCamera() async {
    final returnImage =
        await ImagePicker().pickImage(source: ImageSource.camera);

    if (returnImage != null) {
      await ImageGallerySaver.saveFile(returnImage.path);
      print('Image saved to gallery: $returnImage');
    }

    if (returnImage != null) {
      final croppedImage = await ImageCropper().cropImage(
        sourcePath: returnImage.path,
        aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      );

      if (croppedImage != null) {
        print('Image cropped: ${croppedImage.path}');

        setState(() {
          selectedImage = File(croppedImage.path);
        });

        try {
          final currentUser = FirebaseAuth.instance.currentUser;
          if (currentUser != null) {
            final ref = firebase_storage.FirebaseStorage.instance
                .ref()
                .child('profile_photos')
                .child(currentUser.uid)
                .child('profile_photo.jpg');

            await ref.putFile(selectedImage!);

            final downloadURL = await ref.getDownloadURL();

            await FirebaseFirestore.instance
                .collection('users')
                .doc(currentUser.uid)
                .update({'profilePhotoUrl': downloadURL});

            print(
                'Image uploaded to Firebase Storage and URL saved to Firestore.');
          } else {
            print('Error: currentUser is null.');
          }
        } catch (error) {
          print('Error uploading image: $error');
        }
      } else {
        print('Error: Cropped image is null.');
      }
    } else {
      print('Error: No image selected.');
    }

    Navigator.of(context).pop();
  }
}
