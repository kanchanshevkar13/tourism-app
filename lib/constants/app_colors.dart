import 'package:flutter/material.dart';

class AppColors{
  static const Color backgroundDark = Color(0xff323340);
  static const Color backgroundLight = Color(0xff3C3D4A);
  static const Color whiteColor = Color(0xffFFFFFF);

  static const Color containerColor = Color(0xFF7B71F5);
  static const Color dropdownColor = Color(0xFF8E85F7);
  static const Color textColor = Color(0xFFFFFFFF);
  static const Color tilesColor = Color(0xFFEBEBEB);
  static const Color cardColor = Color(0xFFBA70F0);
  static const Color structureColor = Color(0xFFE2E0FF);
  static const Color darkContainerColor = Color(0xFF3E397B);
  static const Color lightContainerColor = Color(0xFF7B71F5);
  static const Color lightTextGreyColor = Color(0xFF656565);
  static const Color downloadLinkColour = Color(0xFF9C96FB);
  static const Color dividerColor = Color(0xFFFDFDFD);
  static const Color buttonColor = Color(0xFF6C63FF);
  
  static const Color lightGreyColor = Color(0XFF969696);
  static const Color menuGreyColor = Color(0xff939393);
  static const Color darkGreyColor = Color(0xff4A4B57);
  //static const Color containerColor = Color(0xff4A4B57);
  static const Color textGrey = Color(0xffD1D1D1);
  static const Color borderColor = Color(0xff6B6B6B);
  static const Color textformFieldColor = Color(0xff707070);
}