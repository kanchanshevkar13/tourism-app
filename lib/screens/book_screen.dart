import 'package:firebase1/auth_method.dart';
import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/utils/snackbar.dart';
import 'package:firebase1/widgets/custom_button_widget.dart';
import 'package:firebase1/widgets/textformfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

class BookScreen extends StatefulWidget {
  const BookScreen({super.key});

  @override
  State<BookScreen> createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  final TextEditingController destinationController = TextEditingController();
  final TextEditingController date1Controller = TextEditingController();
  final TextEditingController date2Controller = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController memberController = TextEditingController();
  bool isLoading = false;
  final _formKey = GlobalKey<FormState>();

  void dispose() {
    super.dispose();
    destinationController.dispose();
    date1Controller.dispose();
    date2Controller.dispose();
    nameController.dispose();
    memberController.dispose();
  }

  void booktrip() async {
    setState(() {
      isLoading = true;
    });

    try {
      String res = await AuthMethod().booktrip(
        destination: destinationController.text,
        date1: date1Controller.text,
        date2: date2Controller.text,
        name: nameController.text,
        member: memberController.text,
      );

      if (res == "success") {
        setState(() {
          isLoading = false;
        });
        // Navigator.of(context).pushReplacement(
        //     MaterialPageRoute(builder: (context) => HomePage()));
        // ShowSnackBar(context, "Your trip booked successfully");
        // ignore: use_build_context_synchronously
        context.push(Routes.homePage);
        SnackBarWidget.showSnackbar(context, "Your trip booked successfully");
      } else {
        setState(() {
          isLoading = false;
        });
        //ShowSnackBar(context, res);
        SnackBarWidget.showSnackbar(context, res);
      }
    } catch (error) {
      print("Error during booking trip: $error");
      setState(() {
        isLoading = false;
      });
      SnackBarWidget.showSnackbar(context, "An unexpected error occured");
      //ShowSnackBar(context, "An unexpected error occurred");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(20.w),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextformFieldWidget(
                  hintText: "Enter Full Name",
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: nameController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter full name';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Enter Destination",
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: destinationController,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your destination';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Enter Starting Date",
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: date1Controller,
                  useDatePicker: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter starting date';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Enter Ending Date",
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: date2Controller,
                  useDatePicker: true,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter ending date';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  controller: memberController,
                  hintText: "Enter Members",
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  textColor: AppColors.textColor,
                  textInputType: TextInputType.number,
                  inputFormater: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter members';
                    }
                    return null;
                  },
                ),
                CustomOutlinedButton(
                  borderRadius: 5.r,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      booktrip();
                    }
                  },
                  buttonHeight: 50,
                  buttonWidth: double.infinity,
                  backgroundColor: AppColors.darkGreyColor,
                  buttonText: "Plan Trip",
                  fontWeight: FontWeight.bold,
                  textColor: AppColors.textColor,
                  fontSize: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
    // Scaffold(
    //   body: Stack(
    //     children: [
    //       // Background Image
    //       Image.asset(
    //         'assets/images/background.jpg', // Replace with your background image path
    //         fit: BoxFit.fill,
    //         height: double.infinity,
    //         width: double.infinity,
    //       ),
    //       // Transparent Container to Dim the Background
    //       Container(
    //         color:
    //             Colors.black.withOpacity(0.5), // Adjust the opacity as needed
    //         height: double.infinity,
    //         width: double.infinity,
    //       ),
    //       // Trip Planner Content
    //       Center(
    //         child: Padding(
    //           padding: const EdgeInsets.all(16.0),
    //           child: SingleChildScrollView(
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 // Your Trip Planning Widgets Here
    //                 TextFieldInput1(
    //                     textEditingController: nameController,
    //                     hintText: 'Enter full name',
    //                     textInputType: TextInputType.text),
    //                 const SizedBoxWidget(height: 16.0),
    //                 TextFieldInput1(
    //                     textEditingController: destinationController,
    //                     hintText: 'Enter destination',
    //                     textInputType: TextInputType.text),
    //                 const SizedBoxWidget(height: 16.0),
    //                 TextFieldInput1(
    //                     textEditingController: date1Controller,
    //                     hintText: 'Enter starting date',
    //                     textInputType: TextInputType.text),
    //                 const SizedBoxWidget(height: 16.0),
    //                 TextFieldInput1(
    //                     textEditingController: date2Controller,
    //                     hintText: 'Enter ending date',
    //                     textInputType: TextInputType.text),
    //                 const SizedBoxWidget(height: 16.0),
    //                 TextFieldInput1(
    //                     textEditingController: memberController,
    //                     hintText: 'Enter total members',
    //                     textInputType: TextInputType.text),
    //                 const SizedBoxWidget(height: 20.0),

    //                 ElevatedButton(
    //                   onPressed: booktrip,
    //                   child: Text(
    //                     'Plan Trip',
    //                     style: TextStyle(color: Colors.black),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
