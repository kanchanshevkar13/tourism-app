import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';

class RealTimeData extends StatelessWidget {
  RealTimeData({super.key});

  //final ref = FirebaseDatabase.instance.ref('exam');
  final CollectionReference users = FirebaseFirestore.instance.collection('users');


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Real Time Database"),),
      body: FutureBuilder<QuerySnapshot>(
        future: users.get(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          // Process data here
          final List<DocumentSnapshot> documents = snapshot.data!.docs;
          return ListView.builder(
            itemCount: documents.length,
            itemBuilder: (context, index) {
              final Map<String, dynamic> data = documents[index].data() as Map<String, dynamic>;
              return ListTile(
                title: Text(data['name']),
                subtitle: Text(data['email']),
              );
            },
          );
        },
      ),
      
      /*Column(children: [
        Expanded(child: FirebaseAnimatedList(query:ref, itemBuilder: (context, snapshot, animation, index,){
          return ListTile(
            title: Text(snapshot.child('insem').value.toString()),
            subtitle: Text(snapshot.child('endsem').value.toString()),
          );
        }),)
      ]),*/
    );
  }
}