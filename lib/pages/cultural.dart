import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Cultural extends StatefulWidget {
  const Cultural({super.key});

  @override
  State<Cultural> createState() => _CulturalState();
}

class _CulturalState extends State<Cultural> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              CustomIconWidget(
                onPressed: () {
                  context.push(Routes.homePage);
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
              ),
              const SizedBoxWidget(
                width: 20,
              ),
              const TextWidget(
                text: "Cultural",
                color: AppColors.textColor,
              )
            ],
          ),
          //const TextWidget(text: "Cultural",),
          backgroundColor: AppColors.backgroundDark,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.amritImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Amritsar"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.tamilImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Tamilnadu"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.mysoreImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Mysore"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.hampiImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Hampi"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.goaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Goa"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.kolkataImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Kolkata"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.cholaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Chola"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.khajurImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Khajuraho"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.rajsthanImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Rajsthan"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.lucknowImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Lucknow"),
              ],
            ),
          ),
        ));
  }
}
