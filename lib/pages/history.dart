import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class History extends StatefulWidget {
  const History({super.key});

  @override
  State<History> createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              CustomIconWidget(
                onPressed: () {
                  context.push(Routes.homePage);
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
              ),
              const SizedBoxWidget(
                width: 20,
              ),
              const TextWidget(
                text: "Historical",
                color: AppColors.textColor,
              )
            ],
          ),
          //const TextWidget(text: "Historical"),
          backgroundColor: AppColors.backgroundDark,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.tajImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Taj Mahal, Agra"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.agraImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Agra Fort"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.redImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Red Fort"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.minarImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Qutub Minar"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.sikriImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Fatehpur Sikri"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.hawaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Hawa Mahal"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.victoriaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Victroia Mahal"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.tombImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Humayun's Tomb"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.bagImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Jalianwala Bag"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.stupImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Sanchi Stup"),
              ],
            ),
          ),
        ));
  }
}
