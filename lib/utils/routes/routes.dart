import 'package:firebase1/pages/adventure.dart';
import 'package:firebase1/pages/cultural.dart';
import 'package:firebase1/pages/hill.dart';
import 'package:firebase1/pages/history.dart';
import 'package:firebase1/pages/religious.dart';
import 'package:firebase1/pages/tourist.dart';
import 'package:firebase1/pages/wild.dart';
import 'package:firebase1/screens/book_screen.dart';
import 'package:firebase1/screens/home_screen.dart';
import 'package:firebase1/screens/login_screen.dart';
import 'package:firebase1/screens/my_trips_screen.dart';
import 'package:firebase1/screens/profile_screen.dart';
import 'package:firebase1/screens/reset_password.dart';
import 'package:firebase1/screens/sign_up_screen.dart';
import 'package:go_router/go_router.dart';

class Routes {
  static const String loginScreen = '/login';
  static const String homePage = '/homePage';
  static const String signupScreen = '/signupScreen';
  static const String bookScreen = '/bookScreen';
  static const String homeScreen = '/homeScreen';
  static const String profileScreen = '/profileScreen';
  static const String myTripScreen = '/myTripScreen';
  static const String resetPassword = '/resetPassword';

  static const String hillScreen = '/hillScreen';
  static const String adventureScreen = '/adventureScreen';
  static const String culturalScreen = '/culturalScreen';
  static const String historyScreen = '/historyScreen';
  static const String religiousScreen = '/religiousScreen';
  static const String wildScreen = '/wildScreen';

  GoRouter get router => _goRouter;

  late final GoRouter _goRouter = GoRouter(
    initialLocation: homeScreen,
    routes: [
      GoRoute(
        path: loginScreen,
        builder: (context, state) => const LoginScreen(),
      ),
      GoRoute(
        path: homePage,
        builder: (context, state) => const HomePage(),
      ),
      GoRoute(
        path: signupScreen,
        builder: (context, state) => const SignUpScreen(),
      ),
      GoRoute(
        path: bookScreen,
        builder: (context, state) => const BookScreen(),
      ),
      GoRoute(
        path: homeScreen,
        builder: (context, state) => HomeScreen(),
      ),
      GoRoute(
        path: profileScreen,
        builder: (context, state) => const ProfileScreen(),
      ),
      GoRoute(
        path: myTripScreen,
        builder: (context, state) => const MyTrips(),
      ),
      GoRoute(
        path: resetPassword,
        builder: (context, state) => const ResetPassword(),
      ),
      GoRoute(
        path: hillScreen,
        builder: (context, state) => const Hill(),
      ),
      GoRoute(
        path: adventureScreen,
        builder: (context, state) => const Adventure(),
      ),
      GoRoute(
        path: culturalScreen,
        builder: (context, state) => const Cultural(),
      ),
      GoRoute(
        path: historyScreen,
        builder: (context, state) => const History(),
      ),
      GoRoute(
        path: religiousScreen,
        builder: (context, state) => const Religious(),
      ),
      GoRoute(
        path: wildScreen,
        builder: (context, state) => const Wild(),
      )
    ],
  );
}
