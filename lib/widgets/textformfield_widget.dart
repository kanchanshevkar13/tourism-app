// // import 'package:flutter/material.dart';
// // import 'package:flutter/services.dart';
// // import 'package:flutter_screenutil/flutter_screenutil.dart';
// // import 'package:google_fonts/google_fonts.dart';
// // import '../constants/app_colors.dart';

// // class TextformFieldWidget extends StatefulWidget {
// //   const TextformFieldWidget({
// //     super.key,
// //     this.hintText,
// //     this.height,
// //     this.rouneded = 5.0,
// //     this.fontWeight,
// //     this.focusNode,
// //     this.suffixIcon,
// //     this.isSuffixIconShow = false,
// //     this.isRounded = true,
// //     this.backgroundColor,
// //     this.controller,
// //     this.textInputType,
// //     this.inputFormater,
// //     this.onChanged,
// //     this.validator,
// //     this.initialValue,
// //     this.isBorderColor = true,
// //     this.obscureText,
// //     this.hintTextColor,
// //     this.textColor,
// //     this.fontSize = 18,
// //     this.maxLine,
// //     this.border,
// //     this.autovalidateMode,
// //     this.readOnly = false,
// //   });

// //   final String? initialValue;
// //   final String? hintText;
// //   final int? maxLine;
// //   final bool isSuffixIconShow;
// //   final double? height;
// //   final double rouneded;
// //   final bool isRounded;
// //   final double fontSize;
// //   final FontWeight? fontWeight;
// //   final Color? backgroundColor;
// //   final Color? hintTextColor;
// //   final Color? textColor;
// //   final FocusNode? focusNode;
// //   final Widget? suffixIcon;
// //   final Widget? border;
// //   final TextInputType? textInputType;
// //   final TextEditingController? controller;
// //   final List<TextInputFormatter>? inputFormater;
// //   final bool? isBorderColor;
// //   final bool? obscureText;
// //   final Function(String)? onChanged;
// //   final String? Function(String?)? validator;
// //   final AutovalidateMode? autovalidateMode;
// //   final bool readOnly;

// //   @override
// //   State<TextformFieldWidget> createState() => _TextformFieldWidgetState();
// // }

// // class _TextformFieldWidgetState extends State<TextformFieldWidget> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return Container(
// //       decoration: BoxDecoration(
// //         //color: widget.backgroundColor,
// //         borderRadius: widget.isRounded ? BorderRadius.circular(widget.rouneded.r) : null,
// //       ),
// //       height: widget.height?.h,
// //       child: TextFormField(
// //         readOnly: widget.readOnly,
// //         autovalidateMode: widget.autovalidateMode,
// //         maxLines: widget.maxLine ?? 1,
// //         obscureText: widget.obscureText ?? false,
// //         initialValue: widget.initialValue,
// //         cursorColor: AppColors.backgroundDark,
// //         inputFormatters: widget.inputFormater,
// //         keyboardType: widget.textInputType,
// //         autofocus: false,
// //         controller: widget.controller,
// //         focusNode: widget.focusNode,
// //         onChanged: widget.onChanged,
// //         validator: widget.validator,
// //         style: GoogleFonts.kumbhSans(
// //           fontSize: widget.fontSize.sp,
// //           fontWeight: widget.fontWeight,
// //           color: widget.textColor,
// //         ),
// //         decoration: InputDecoration(
// //           contentPadding: EdgeInsets.all(10.h),
// //           hintText: widget.hintText,
// //           suffixIcon: widget.suffixIcon,
// //           hintStyle: GoogleFonts.kumbhSans(
// //             fontSize: widget.fontSize.sp,
// //             fontWeight: widget.fontWeight,
// //             color: widget.hintTextColor,
// //           ),
// //           errorBorder: OutlineInputBorder(
// //             borderRadius: BorderRadius.circular(widget.rouneded.r),
// //             borderSide:BorderSide(
// //               width: 1.w,
// //               color: Colors.red,
// //             ),
// //           ),
// //           focusedErrorBorder: OutlineInputBorder(
// //             borderRadius: BorderRadius.circular(widget.rouneded.r),
// //             borderSide: BorderSide(width: 1.w, color: Colors.red),
// //           ),
// //           enabledBorder: OutlineInputBorder(
// //             borderRadius: BorderRadius.circular(widget.rouneded.r),
// //             borderSide: BorderSide(
// //               width: 0.5.w,
// //               color: (widget.isBorderColor ?? false) ? AppColors.backgroundLight : AppColors.backgroundLight,
// //             ),
// //           ),
// //           focusedBorder: widget.isRounded
// //               ? OutlineInputBorder(
// //                   borderRadius: BorderRadius.circular(widget.rouneded.r),
// //                   borderSide: BorderSide(
// //                     width: 1.w,
// //                     color: (widget.isBorderColor ?? false) ? AppColors.lightTextGreyColor : Colors.red,
// //                   ),
// //                 )
// //               : OutlineInputBorder(
// //                   borderSide: BorderSide(
// //                     width: 1.w,
// //                     color: (widget.isBorderColor ?? false) ? AppColors.lightTextGreyColor : Colors.red,
// //                   ),
// //                 ),
// //         ),
// //       ),
// //     );
// //   }
// // }

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:intl/intl.dart'; // Add this to format the date
// import '../constants/app_colors.dart';

// class TextformFieldWidget extends StatefulWidget {
//   const TextformFieldWidget({
//     super.key,
//     this.hintText,
//     this.height,
//     this.rouneded = 5.0,
//     this.fontWeight,
//     this.focusNode,
//     this.suffixIcon,
//     this.isSuffixIconShow = false,
//     this.isRounded = true,
//     this.backgroundColor,
//     this.controller,
//     this.textInputType,
//     this.inputFormater,
//     this.onChanged,
//     this.validator,
//     this.initialValue,
//     this.isBorderColor = true,
//     this.obscureText,
//     this.hintTextColor,
//     this.textColor,
//     this.fontSize = 18,
//     this.maxLine,
//     this.border,
//     this.autovalidateMode,
//     this.readOnly = false,
//     this.useDatePicker = false, // Add this parameter
//   });

//   final String? initialValue;
//   final String? hintText;
//   final int? maxLine;
//   final bool isSuffixIconShow;
//   final double? height;
//   final double rouneded;
//   final bool isRounded;
//   final double fontSize;
//   final FontWeight? fontWeight;
//   final Color? backgroundColor;
//   final Color? hintTextColor;
//   final Color? textColor;
//   final FocusNode? focusNode;
//   final Widget? suffixIcon;
//   final Widget? border;
//   final TextInputType? textInputType;
//   final TextEditingController? controller;
//   final List<TextInputFormatter>? inputFormater;
//   final bool? isBorderColor;
//   final bool? obscureText;
//   final Function(String)? onChanged;
//   final String? Function(String?)? validator;
//   final AutovalidateMode? autovalidateMode;
//   final bool readOnly;
//   final bool useDatePicker; // Add this parameter

//   @override
//   State<TextformFieldWidget> createState() => _TextformFieldWidgetState();
// }

// class _TextformFieldWidgetState extends State<TextformFieldWidget> {
//   Future<void> _selectDate(BuildContext context) async {
//     final DateTime? picked = await showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(1900),
//       lastDate: DateTime(2101),
//     );
//     if (picked != null) {
//       widget.controller?.text = DateFormat('dd/MM/yyyy').format(picked);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: BoxDecoration(
//         borderRadius: widget.isRounded ? BorderRadius.circular(widget.rouneded.r) : null,
//       ),
//       height: widget.height?.h,
//       child: TextFormField(
//         readOnly: widget.readOnly || widget.useDatePicker,
//         autovalidateMode: widget.autovalidateMode,
//         maxLines: widget.maxLine ?? 1,
//         obscureText: widget.obscureText ?? false,
//         initialValue: widget.initialValue,
//         cursorColor: AppColors.backgroundDark,
//         inputFormatters: widget.inputFormater,
//         keyboardType: widget.textInputType,
//         autofocus: false,
//         controller: widget.controller,
//         focusNode: widget.focusNode,
//         onChanged: widget.onChanged,
//         validator: widget.validator,
//         style: GoogleFonts.kumbhSans(
//           fontSize: widget.fontSize.sp,
//           fontWeight: widget.fontWeight,
//           color: widget.textColor,
//         ),
//         decoration: InputDecoration(
//           contentPadding: EdgeInsets.all(10.h),
//           hintText: widget.hintText,
//           suffixIcon: widget.suffixIcon,
//           hintStyle: GoogleFonts.kumbhSans(
//             fontSize: widget.fontSize.sp,
//             fontWeight: widget.fontWeight,
//             color: widget.hintTextColor,
//           ),
//           errorBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(widget.rouneded.r),
//             borderSide: BorderSide(
//               width: 1.w,
//               color: Colors.red,
//             ),
//           ),
//           focusedErrorBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(widget.rouneded.r),
//             borderSide: BorderSide(width: 1.w, color: Colors.red),
//           ),
//           enabledBorder: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(widget.rouneded.r),
//             borderSide: BorderSide(
//               width: 0.5.w,
//               color: (widget.isBorderColor ?? false) ? AppColors.backgroundLight : AppColors.backgroundLight,
//             ),
//           ),
//           focusedBorder: widget.isRounded
//               ? OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(widget.rouneded.r),
//                   borderSide: BorderSide(
//                     width: 1.w,
//                     color: (widget.isBorderColor ?? false) ? AppColors.lightTextGreyColor : Colors.red,
//                   ),
//                 )
//               : OutlineInputBorder(
//                   borderSide: BorderSide(
//                     width: 1.w,
//                     color: (widget.isBorderColor ?? false) ? AppColors.lightTextGreyColor : Colors.red,
//                   ),
//                 ),
//         ),
//         onTap: widget.useDatePicker ? () => _selectDate(context) : null, // Add this line
//       ),
//     );
//   }
// }

import 'package:firebase1/widgets/icon_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart'; // Add this to format the date
import '../constants/app_colors.dart';

class TextformFieldWidget extends StatefulWidget {
  const TextformFieldWidget({
    super.key,
    this.hintText,
    this.height,
    this.rouneded = 5.0,
    this.fontWeight,
    this.focusNode,
    this.suffixIcon,
    this.isSuffixIconShow = false,
    this.isRounded = true,
    this.backgroundColor,
    this.controller,
    this.textInputType,
    this.inputFormater,
    this.onChanged,
    this.validator,
    this.initialValue,
    this.isBorderColor = true,
    this.obscureText,
    this.hintTextColor,
    this.textColor,
    this.fontSize = 18,
    this.maxLine,
    this.border,
    this.autovalidateMode,
    this.readOnly = false,
    this.useDatePicker = false, // Add this parameter
  });

  final String? initialValue;
  final String? hintText;
  final int? maxLine;
  final bool isSuffixIconShow;
  final double? height;
  final double rouneded;
  final bool isRounded;
  final double fontSize;
  final FontWeight? fontWeight;
  final Color? backgroundColor;
  final Color? hintTextColor;
  final Color? textColor;
  final FocusNode? focusNode;
  final Widget? suffixIcon;
  final Widget? border;
  final TextInputType? textInputType;
  final TextEditingController? controller;
  final List<TextInputFormatter>? inputFormater;
  final bool? isBorderColor;
  final bool? obscureText;
  final Function(String)? onChanged;
  final String? Function(String?)? validator;
  final AutovalidateMode? autovalidateMode;
  final bool readOnly;
  final bool useDatePicker; // Add this parameter

  @override
  State<TextformFieldWidget> createState() => _TextformFieldWidgetState();
}

class _TextformFieldWidgetState extends State<TextformFieldWidget> {
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2101),
    );
    if (picked != null) {
      widget.controller?.text = DateFormat('dd/MM/yyyy').format(picked);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius:
            widget.isRounded ? BorderRadius.circular(widget.rouneded.r) : null,
      ),
      height: widget.height?.h,
      child: TextFormField(
        readOnly: widget.readOnly || widget.useDatePicker,
        autovalidateMode: widget.autovalidateMode,
        maxLines: widget.maxLine ?? 1,
        obscureText: widget.obscureText ?? false,
        initialValue: widget.initialValue,
        cursorColor: AppColors.lightTextGreyColor,
        inputFormatters: widget.inputFormater,
        keyboardType: widget.textInputType,
        autofocus: false,
        controller: widget.controller,
        focusNode: widget.focusNode,
        onChanged: widget.onChanged,
        validator: widget.validator,
        style: GoogleFonts.kumbhSans(
          fontSize: widget.fontSize.sp,
          fontWeight: widget.fontWeight,
          color: widget.textColor,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10.h),
          hintText: widget.hintText,
          suffixIcon: widget.useDatePicker
              ? CustomIconWidget(
                  onPressed: () {
                    _selectDate(context);
                  },
                  iconData: Icons.calendar_today,
                  color: AppColors.textColor,
                )
              // IconButton(
              //     icon: Icon(Icons.calendar_today),
              //     onPressed: () => _selectDate(context),
              //   )
              : widget.suffixIcon,
          hintStyle: GoogleFonts.kumbhSans(
            fontSize: widget.fontSize.sp,
            fontWeight: widget.fontWeight,
            color: widget.hintTextColor,
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.rouneded.r),
            borderSide: BorderSide(
              width: 1.w,
              color: Colors.red,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.rouneded.r),
            borderSide: BorderSide(width: 1.w, color: Colors.red),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(widget.rouneded.r),
            borderSide: BorderSide(
              width: 0.5.w,
              color: (widget.isBorderColor ?? false)
                  ? AppColors.backgroundLight
                  : AppColors.backgroundLight,
            ),
          ),
          focusedBorder: widget.isRounded
              ? OutlineInputBorder(
                  borderRadius: BorderRadius.circular(widget.rouneded.r),
                  borderSide: BorderSide(
                    width: 1.w,
                    color: (widget.isBorderColor ?? false)
                        ? AppColors.lightTextGreyColor
                        : Colors.red,
                  ),
                )
              : OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1.w,
                    color: (widget.isBorderColor ?? false)
                        ? AppColors.lightTextGreyColor
                        : Colors.red,
                  ),
                ),
        ),
        onTap: widget.useDatePicker && !widget.readOnly
            ? () => _selectDate(context)
            : null, // Add this line
      ),
    );
  }
}
