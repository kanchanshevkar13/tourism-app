import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/widgets/circular_progress_indicator_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomOutlinedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String? buttonText;
  final bool? inProgress;
  final double? buttonHeight;
  final double? buttonWidth;
  final double? textSize;
  final Color? textColor;
  final double? fontSize;
  final FontWeight? fontWeight;
  final Color? backgroundColor;
  final double? borderRadius;
  final BorderSide customBorderSide;
  final Widget? suffixIcon;
  final Widget? prefixIcon;

  const CustomOutlinedButton({
    Key? key,
    required this.onPressed,
    this.buttonText,
    this.inProgress,
    this.buttonHeight,
    this.buttonWidth,
    this.textColor,
    this.fontWeight,
    this.textSize,
    this.backgroundColor,
    this.borderRadius,
    this.customBorderSide = BorderSide.none,
    this.fontSize,
    this.suffixIcon,
    this.prefixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: buttonHeight ?? 40.h,
      width: buttonWidth ?? 300.w,
      child: ElevatedButton(
        style: ButtonStyle(
          shape: WidgetStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius ?? 0),
              ),
              side: customBorderSide,
            ),
          ),
          backgroundColor: WidgetStateProperty.all<Color?>(backgroundColor),
        ),
        onPressed: onPressed,
        child: inProgress ?? false
            ? const CustomCircularProgressIndidator()
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (prefixIcon != null) ...[
                    prefixIcon!,
                    SizedBoxWidget(
                        width: 5.w), // Space between prefix icon and text
                  ],
                  Expanded(
                    child: Center(
                      child: TextWidget(
                        text: buttonText,
                        fontSize: fontSize,
                        fontWeight: fontWeight,
                        color: textColor,
                      ),
                    ),
                  ),
                  if (suffixIcon != null) ...[
                    SizedBoxWidget(
                      width: 5.w,
                    ), // Space between text and suffix icon
                    suffixIcon!,
                  ],
                ],
              ),
      ),
    );
  }
}
