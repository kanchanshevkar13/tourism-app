import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthMethod {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<String> signUpUser({
    required String email,
    required String password,
    required String confirmpassword,
    required String address,
    required String name,
    required String age,
    required String mobile,
  }) async {
    try {
      if (email.isEmpty ||
          password.isEmpty ||
          confirmpassword.isEmpty ||
          name.isEmpty ||
          address.isEmpty ||
          mobile.isEmpty ||
          age.isEmpty) {
        return "Please fill in all the fields";
      } else if (password != confirmpassword) {
        return "please confirm your password";
      } else if(mobile.length!=10){
        return "please enter valid mobile number";
      }
      

      UserCredential cred = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      print("User UID: ${cred.user!.uid}");

      await _firestore.collection("users").doc(cred.user!.uid).set({
        'name': name,
        'address': address,
        'email': email,
        'password': password,
        'age': age,
        'mobile': mobile,
        // Remove 'password' field
      });
      return "success";
    } catch (error) {
      print("Error during signUpUser: $error");
      return "The email address is badly formatted.";
    }
  }

Future<String> booktrip({
  required String name,
  required String destination,
  required String date1,
  required String date2,
  required String member,
}) async {
  try {
    if (name.isEmpty || destination.isEmpty) {
      return "Please fill required fields";
    }

    User? user = FirebaseAuth.instance.currentUser;

    if (user != null) {
      // Add trip information to the user's document
      await _firestore.collection('users').doc(user.uid).update({
        "trips": FieldValue.arrayUnion([
          {
            "name": name,
            "destination": destination,
            "start date": date1,
            "end date": date2,
            "total members": member,
          }
        ]),
      });

      // If you want to add trip information to a separate collection, uncomment the following code
      /*
      await _firestore.collection('trips').add({
        "name": name,
        "destination": destination,
        "start date": date1,
        "end date": date2,
        "total members": member,
        "user_uid": user.uid,
      });
      */
    } else {
      return "User not authenticated";
    }

    return "success";
  } catch (error) {
    print("Error during bookTrip: $error");
    return "An error occurred while booking the trip.";
  }
}

  //for login user

  Future<String> loginUser({
    required String email,
    required String password,
  }) async {
    try {
      if (email.isEmpty || password.isEmpty) {
        return "Please provide both email and password";
      }

      UserCredential cred = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      print("User UID: ${cred.user!.uid}");

      // You can perform additional checks or actions after a successful login if needed
      return "success";
    } catch (error) {
      print("Error during loginUser: $error");
      return "Invalid email or password";
    }
  }
}
