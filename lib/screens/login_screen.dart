import 'package:firebase1/auth_method.dart';
import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/utils/snackbar.dart';
import 'package:firebase1/widgets/custom_button_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:firebase1/widgets/textformfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;
  bool isObscured = true;

  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  void loginUser() async {
    setState(() {
      isLoading = true;
    });

    try {
      String res = await AuthMethod().loginUser(
        email: emailController.text,
        password: passwordController.text,
      );

      if (res == "success") {
        setState(() {
          isLoading = false;
        });
        context.push(Routes.bookScreen);
        SnackBarWidget.showSnackbar(context, "You have logged in successfully");
        //showSnackbar(context, "You have logged in successfully");
      } else {
        setState(() {
          isLoading = false;
        });
        SnackBarWidget.showSnackbar(context, res);
        //ShowSnackBar(context, res);
      }
    } catch (error) {
      print("Error during login: $error");
      setState(() {
        isLoading = false;
      });
      SnackBarWidget.showSnackbar(context, "An unexpected error occured");
      //ShowSnackBar(context, "An unexpected error occurred");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.all(20.w),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextformFieldWidget(
                hintText: "Enter Your Email",
                controller: emailController,
                textColor: AppColors.textColor,
                hintTextColor: AppColors.lightTextGreyColor,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your email address';
                  }
                  if (!RegExp(
                          r'^[\w-]+(\.[\w-]+)*@(?![0-9_])[a-zA-Z0-9_]+(\.[a-zA-Z]{2,7})$')
                      .hasMatch(value)) {
                    return 'Please enter a valid email address';
                  }
                  return null;
                },
              ),
              const SizedBoxWidget(
                height: 20,
              ),
              TextformFieldWidget(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                hintText: "Enter Your Password",
                controller: passwordController,
                textColor: AppColors.textColor,
                hintTextColor: AppColors.lightTextGreyColor,
                obscureText: isObscured,
                suffixIcon: IconButton(
                  icon: Icon(
                    isObscured ? Icons.visibility_off : Icons.visibility,
                  ),
                  onPressed: () {
                    setState(() {
                      isObscured = !isObscured; // Toggle visibility
                    });
                  },
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  }
                  if (!RegExp(r'^(?=.*[A-Z])').hasMatch(value)) {
                    return 'Password must contain at least one capital letter';
                  }
                  if (!RegExp(r'^(?=.*[!@#$%^&*(),.?":{}|<>])')
                      .hasMatch(value)) {
                    return 'Password must contain at least one special character';
                  }
                  return null;
                },
              ),
              const SizedBoxWidget(
                height: 30,
              ),
              CustomOutlinedButton(
                borderRadius: 5.r,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    loginUser();
                  }
                },
                buttonHeight: 50,
                buttonWidth: double.infinity,
                backgroundColor: AppColors.darkGreyColor,
                buttonText: "Login",
                textColor: AppColors.textColor,
                fontSize: 18,
              ),
              SizedBoxWidget(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const TextWidget(
                    text: "Don't have an account? ",
                    fontSize: 16,
                    color: AppColors.textColor,
                  ),
                  InkWell(
                    onTap: () {
                      context.push(Routes.signupScreen);
                    },
                    child: const TextWidget(
                      text: "SignUp",
                      color: AppColors.textColor,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      )),
    );
    // Scaffold(
    //   resizeToAvoidBottomInset: false,
    //   body: SafeArea(
    //       child: SizedBox(
    //     child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
    //       SizedBox(
    //         height: height / 3,
    //       ),
    // TextformFieldWidget(
    //   hintText: "Enter Your Email",
    //   controller: emailController,
    //   textColor: AppColors.textColor,
    //   hintTextColor: AppColors.lightTextGreyColor,
    //   autovalidateMode: AutovalidateMode.onUserInteraction,
    //   validator: (value) {
    //     if (value == null || value.isEmpty) {
    //       return 'Please enter your email address';
    //     }
    //     if (!RegExp(
    //             r'^[\w-]+(\.[\w-]+)*@(?![0-9_])[a-zA-Z0-9_]+(\.[a-zA-Z]{2,7})$')
    //         .hasMatch(value)) {
    //       return 'Please enter a valid email address';
    //     }
    //     return null;
    //   },
    // ),
    //       // TextFieldInput(
    //       //     textEditingController: emailController,
    //       //     hintText: 'Enter Your email',
    //       //     textInputType: TextInputType.text,),
    //       SizedBox(height: 20),
    // TextformFieldWidget(
    //   autovalidateMode: AutovalidateMode.onUserInteraction,
    //   hintText: "Enter Your Password",
    //   controller: passwordController,
    //   textColor: AppColors.textColor,
    //   hintTextColor: AppColors.lightTextGreyColor,
    //   obscureText: true,
    //   validator: (value) {
    //     if (value == null || value.isEmpty) {
    //       return 'Please enter your password';
    //     }
    //     if (!RegExp(r'^(?=.*[A-Z])').hasMatch(value)) {
    //       return 'Password must contain at least one capital letter';
    //     }
    //     if (!RegExp(r'^(?=.*[!@#$%^&*(),.?":{}|<>])').hasMatch(value)) {
    //       return 'Password must contain at least one special character';
    //     }
    //     return null;
    //   },
    // ),
    //       // TextFieldInput(
    //       //   textEditingController: passwordController,
    //       //   hintText: 'Enter Your password',
    //       //   textInputType: TextInputType.text,
    //       //   isPass: true,
    //       // ),
    //       SizedBox(height: 20),
    //       InkWell(
    //         onTap: loginUser,
    //         child: Padding(
    //           padding: const EdgeInsets.all(8.0),
    //           child: Container(
    //             alignment: Alignment.center,
    //             padding: const EdgeInsets.symmetric(vertical: 12),
    //             decoration: const ShapeDecoration(
    //               shape: RoundedRectangleBorder(
    //                   borderRadius: BorderRadius.all(Radius.circular(4))),
    //               color: Colors.blue,
    //             ),
    //             child: const Text('Login'),
    //           ),
    //         ),
    //       ),
    //       SizedBox(
    //         height: height / 3.1,
    //       ),
    //       Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         children: [
    //           const Text("Don't have an account? "),
    //           GestureDetector(
    //             onTap: () {
    //               Navigator.of(context).push(MaterialPageRoute(
    //                 builder: (context) => const SignUpScreen(),
    //               ));
    //             },
    //             child: const Text(
    //               'SignUp',
    //               style: TextStyle(fontWeight: FontWeight.bold),
    //             ),
    //           )
    //         ],
    //       )
    //     ]),
    //   )),
    // );
  }
}
