import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomIconWidget extends StatelessWidget {
  final IconData? iconData;
  final double? size;
  final Color? color;
  final void Function() onPressed;

  const CustomIconWidget(
      {super.key,
      this.iconData,
      this.size,
      this.color,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Icon(
        iconData,
        size: size ?? 20.sp,
        color: color,
      ),
    );
  }
}
