import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/app_images.dart';
import 'package:firebase1/constants/images_container.dart';
import 'package:firebase1/constants/text_container.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/widgets/icon_widget.dart';
import 'package:firebase1/widgets/sized_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class Religious extends StatefulWidget {
  const Religious({super.key});

  @override
  State<Religious> createState() => _ReligiousState();
}

class _ReligiousState extends State<Religious> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.backgroundDark,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              CustomIconWidget(
                onPressed: () {
                  context.push(Routes.homePage);
                },
                iconData: Icons.arrow_back,
                color: AppColors.textColor,
              ),
              const SizedBoxWidget(
                width: 20,
              ),
              const TextWidget(
                text: "Religious",
                color: AppColors.textColor,
              )
            ],
          ),
          //const TextWidget(text: "Religious"),
          backgroundColor: AppColors.backgroundDark,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.vaishnodeviImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Vaishnodevi Temple, Jammu & Kashmir"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.goldenImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Golden Temple, Amrutsar"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.sunImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Sun Temple, Konark"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.jagannathImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Jagannath Temple, Puri"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.rankapurImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Rankapur Temple, Rajasthan"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.dwarkaImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Dwarka, Gujrat"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.amarnathImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Amarnath Caves, Jammu & Kashmir"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.mathuraImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Mathura, Uttar Pradesh"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.maduraiImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Madurai, Tamilnadu"),
                ImageContainer(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    image: const AssetImage(AppImages.somnathImage)),
                TextContainer(
                    height: 70,
                    width: MediaQuery.of(context).size.width,
                    text: "Somnath Jyotirling, Gujrat"),
              ],
            ),
          ),
        ));
  }
}
