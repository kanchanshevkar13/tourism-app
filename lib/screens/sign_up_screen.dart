import 'package:firebase1/auth_method.dart';
import 'package:firebase1/constants/app_colors.dart';
import 'package:firebase1/constants/textwidget.dart';
import 'package:firebase1/utils/routes/routes.dart';
import 'package:firebase1/utils/snackbar.dart';
import 'package:firebase1/widgets/custom_button_widget.dart';
import 'package:firebase1/widgets/textformfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';

// class SignUpScreen extends StatefulWidget {
//   const SignUpScreen({super.key});

//   @override
//   State<SignUpScreen> createState() => _SignUpScreenState();
// }

// class _SignUpScreenState extends State<SignUpScreen> {
//   final TextEditingController nameController = TextEditingController();
//   final TextEditingController emailController = TextEditingController();
//   final TextEditingController passwordController = TextEditingController();
//   final TextEditingController confirmpasswordController =
//       TextEditingController();
//   final TextEditingController addressController = TextEditingController();
//   final TextEditingController mobileController = TextEditingController();
//   final TextEditingController dobController = TextEditingController();
//   bool isLoading = false;
//   final _formKey = GlobalKey<FormState>();

//   void dispose() {
//     super.dispose();
//     emailController.dispose();
//     passwordController.dispose();
//     nameController.dispose();
//     addressController.dispose();
//     mobileController.dispose();
//     dobController.dispose();
//     confirmpasswordController.dispose();
//   }

//   void signupUser() async {
//     setState(() {
//       isLoading = true;
//     });

//     try {
//       String res = await AuthMethod().signUpUser(
//         email: emailController.text,
//         password: passwordController.text,
//         address: addressController.text,
//         name: nameController.text,
//         mobile: mobileController.text,
//         age: dobController.text,
//         confirmpassword: confirmpasswordController.text,
//       );

//       if (res == "success") {
//         setState(() {
//           isLoading = false;
//         });
//         context.push(Routes.homePage);

//         ShowSnackBar(context, "Your account created successfully");
//       } else {
//         setState(() {
//           isLoading = false;
//         });
//         ShowSnackBar(context, res);
//       }
//     } catch (error) {
//       print("Error during signup: $error");
//       setState(() {
//         isLoading = false;
//       });
//       ShowSnackBar(context, "An unexpected error occurred");
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: AppColors.backgroundDark,
//       body: SafeArea(
//           child: Padding(
//         padding: EdgeInsets.all(20.w),
//         child: Form(
//           key: _formKey,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               TextformFieldWidget(
//                 hintText: "Enter Your Name",
//                 controller: nameController,
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please enter your name';
//                   }
//                   return null;
//                 },
//               ),
//               TextformFieldWidget(
//                 hintText: "Enter Your Email",
//                 controller: emailController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please enter your email address';
//                   }
//                   if (!RegExp(
//                           r'^[\w-]+(\.[\w-]+)*@(?![0-9_])[a-zA-Z0-9_]+(\.[a-zA-Z]{2,7})$')
//                       .hasMatch(value)) {
//                     return 'Please enter a valid email address';
//                   }
//                   return null;
//                 },
//               ),
//               TextformFieldWidget(
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 hintText: "Enter Your Password",
//                 controller: passwordController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 obscureText: true,
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please enter your password';
//                   }
//                   if (!RegExp(r'^(?=.*[A-Z])').hasMatch(value)) {
//                     return 'Password must contain at least one capital letter';
//                   }
//                   if (!RegExp(r'^(?=.*[!@#$%^&*(),.?":{}|<>])')
//                       .hasMatch(value)) {
//                     return 'Password must contain at least one special character';
//                   }
//                   return null;
//                 },
//               ),
//               TextformFieldWidget(
//                 hintText: "Confirm Your Password",
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 controller: confirmpasswordController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please confirm your password';
//                   }
//                   if (value != passwordController.text) {
//                     return 'Passwords do not match';
//                   }
//                   return null;
//                 },
//                 obscureText: true,
//               ),
//               TextformFieldWidget(
//                 hintText: "Enter Your Address",
//                 controller: addressController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//               ),
//               TextformFieldWidget(
//                 hintText: "Enter Your Mobile No",
//                 controller: mobileController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 inputFormater: [
//                   LengthLimitingTextInputFormatter(10),
//                   FilteringTextInputFormatter.digitsOnly
//                 ],
//                 textInputType: TextInputType.number,
//                 validator: (value) {
//                   if (value == null || value.isEmpty) {
//                     return 'Please enter your phone number';
//                   }
//                   if (value.length != 10) {
//                     return 'Please enter a valid 10-digit phone number';
//                   }
//                   if (value == '0000000000') {
//                     return 'Phone number cannot be 10 consecutive zeros';
//                   }
//                   return null;
//                 },
//               ),
//               TextformFieldWidget(
//                 hintText: "Enter Your DOB",
//                 controller: dobController,
//                 textColor: AppColors.textColor,
//                 hintTextColor: AppColors.lightTextGreyColor,
//                 useDatePicker: true,
//               ),
//               CustomOutlinedButton(
//                 borderRadius: 5.r,
//                 onPressed: () {
//                   if (_formKey.currentState!.validate()) {
//                     signupUser;
//                   }
//                 },
//                 buttonHeight: 50,
//                 buttonWidth: double.infinity,
//                 backgroundColor: AppColors.darkGreyColor,
//                 buttonText: "Sign Up",
//                 textColor: AppColors.textColor,
//                 fontSize: 18,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   const TextWidget(
//                     text: "Already have an account? ",
//                     fontSize: 16,
//                     color: AppColors.textColor,
//                   ),
//                   InkWell(
//                     onTap: () {
//                       context.push(Routes.loginScreen);
//                     },
//                     child: const TextWidget(
//                       text: "Login",
//                       color: AppColors.textColor,
//                       fontSize: 16,
//                       fontWeight: FontWeight.bold,
//                     ),
//                   )
//                 ],
//               )
//             ],
//           ),
//         ),
//       )),
//     );
//   }
// }

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmpasswordController =
      TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController mobileController = TextEditingController();
  final TextEditingController dobController = TextEditingController();
  bool isLoading = false;
  bool isObscured = true;
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    emailController.dispose();
    passwordController.dispose();
    nameController.dispose();
    addressController.dispose();
    mobileController.dispose();
    dobController.dispose();
    confirmpasswordController.dispose();
  }

  void signupUser() async {
    setState(() {
      isLoading = true;
    });

    try {
      String res = await AuthMethod().signUpUser(
        email: emailController.text,
        password: passwordController.text,
        address: addressController.text,
        name: nameController.text,
        mobile: mobileController.text,
        age: dobController.text,
        confirmpassword: confirmpasswordController.text,
      );

      if (res == "success") {
        setState(() {
          isLoading = false;
        });
        context.push(Routes.homePage);
        SnackBarWidget.showSnackbar(
            context, "Your account created successfully");
        //ShowSnackBar(context, "Your account created successfully");
      } else {
        setState(() {
          isLoading = false;
        });
        SnackBarWidget.showSnackbar(context, res);
        //ShowSnackBar(context, res);
      }
    } catch (error) {
      print("Error during signup: $error");
      setState(() {
        isLoading = false;
      });
      SnackBarWidget.showSnackbar(context, "An unexpected error occured");
      //ShowSnackBar(context, "An unexpected error occurred");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundDark,
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20.w),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextformFieldWidget(
                  hintText: "Enter Your Full Name",
                  controller: nameController,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your full name';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Enter Your Email",
                  controller: emailController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your email address';
                    }
                    if (!RegExp(
                      r'^[\w-]+(\.[\w-]+)*@(?![0-9_])[a-zA-Z0-9_]+(\.[a-zA-Z]{2,7})$',
                    ).hasMatch(value)) {
                      return 'Please enter a valid email address';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  hintText: "Enter Your Password",
                  controller: passwordController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  obscureText: isObscured,
                  suffixIcon: IconButton(
                    icon: Icon(
                      isObscured ? Icons.visibility_off : Icons.visibility,
                    ),
                    onPressed: () {
                      setState(() {
                        isObscured = !isObscured; // Toggle visibility
                      });
                    },
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your password';
                    }
                    if (!RegExp(r'^(?=.*[A-Z])').hasMatch(value)) {
                      return 'Password must contain at least one capital letter';
                    }
                    if (!RegExp(r'^(?=.*[!@#$%^&*(),.?":{}|<>])')
                        .hasMatch(value)) {
                      return 'Password must contain at least one special character';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Confirm Your Password",
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: confirmpasswordController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please confirm your password';
                    }
                    if (value != passwordController.text) {
                      return 'Passwords do not match';
                    }
                    return null;
                  },
                  obscureText: isObscured,
                  suffixIcon: IconButton(
                    icon: Icon(
                      isObscured ? Icons.visibility_off : Icons.visibility,
                    ),
                    onPressed: () {
                      setState(() {
                        isObscured = !isObscured; // Toggle visibility
                      });
                    },
                  ),
                ),
                TextformFieldWidget(
                  hintText: "Enter Your Address",
                  controller: addressController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                ),
                TextformFieldWidget(
                  hintText: "Enter Your Mobile No",
                  controller: mobileController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  inputFormater: [
                    LengthLimitingTextInputFormatter(10),
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  textInputType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your phone number';
                    }
                    if (value.length != 10) {
                      return 'Please enter a valid 10-digit phone number';
                    }
                    if (value == '0000000000') {
                      return 'Phone number cannot be 10 consecutive zeros';
                    }
                    return null;
                  },
                ),
                TextformFieldWidget(
                  hintText: "Enter Your DOB",
                  controller: dobController,
                  textColor: AppColors.textColor,
                  hintTextColor: AppColors.lightTextGreyColor,
                  useDatePicker: true,
                ),
                CustomOutlinedButton(
                  borderRadius: 5.r,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      signupUser();
                    }
                  },
                  buttonHeight: 50,
                  buttonWidth: double.infinity,
                  backgroundColor: AppColors.darkGreyColor,
                  buttonText: "Sign Up",
                  fontWeight: FontWeight.bold,
                  textColor: AppColors.textColor,
                  fontSize: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const TextWidget(
                      text: "Already have an account? ",
                      fontSize: 16,
                      color: AppColors.textColor,
                    ),
                    InkWell(
                      onTap: () {
                        context.push(Routes.loginScreen);
                      },
                      child: const TextWidget(
                        text: "Login",
                        color: AppColors.textColor,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
