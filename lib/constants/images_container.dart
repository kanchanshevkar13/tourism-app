import 'package:firebase1/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ImageContainer extends StatelessWidget {
  final double height;
  final double width;
  final AssetImage image;
  const ImageContainer({
    super.key,
    required this.height,
    required this.width,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        height: height.h,
        width: width.w,
        decoration: BoxDecoration(
          // boxShadow: [
          //   BoxShadow(
          //     color: AppColors.whiteColor.withOpacity(0.3),
          //     blurRadius: 2,
          //     spreadRadius: 1,
          //   ),
          // ],
          image: DecorationImage(
            image: image,
            fit: BoxFit.fill,
          ),
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}
